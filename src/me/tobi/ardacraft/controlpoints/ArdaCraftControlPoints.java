package me.tobi.ardacraft.controlpoints;

import org.bukkit.plugin.java.JavaPlugin;

public class ArdaCraftControlPoints extends JavaPlugin{
	
	private static ArdaCraftControlPoints plugin;
	
	@Override
	public void onEnable() {
		System.out.println("Enabling Controlpoints...");
		plugin = this;
		getCommand("controlpoint").setExecutor(new CmdControlpoint());
		getServer().getPluginManager().registerEvents(new ControlPointListener(), this);
		getServer().getPluginManager().registerEvents(new GuardManager.AttackListener(), this);
		ControlPointScheduler cps = new ControlPointScheduler();
		cps.run();
	}
	
	public static ArdaCraftControlPoints getPlugin() {
		return plugin;
	}
	
	
	
}
