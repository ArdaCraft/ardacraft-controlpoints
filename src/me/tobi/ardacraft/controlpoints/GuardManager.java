package me.tobi.ardacraft.controlpoints;

import me.tobi.ardacraft.api.classes.ControlPoint;
import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.api.classes.Region;
import me.tobi.ardacraft.api.classes.Utils;
import me.tobi.ardacraft.controlpoints.utils.RandomUtils;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.ArrayList;
import java.util.List;

public class GuardManager {

    public static void onPlayerNearControlPoint(Region rg, Player p){

        if((Rasse.get(p).getAttitude() == Utils.Attitude.GOOD && rg.getCpState() < 0)
                || (Rasse.get(p).getAttitude() == Utils.Attitude.BAD && rg.getCpState() > 0)){

            attackTarget(rg, p);
        }

    }

    public static void onNoPlayersNearControlPoint(Region rg){
        for(CitizensGuard guard : getOrFixGuardsFromControlPoint(rg)){
            guard.clearAttacking();
        }
    }

    public static void guardStroll(List<Region> rgs){
        for(Region rg : rgs){
            List<CitizensGuard> guards =  getOrFixGuardsFromControlPoint(rg);

            guards.get(RandomUtils.randomInt(0, guards.size() - 1)).stroll(rg);
        }
    }

    public static void clearAttacks(Region rg){
        for(CitizensGuard guard : getOrFixGuardsFromControlPoint(rg)){
            guard.clearAttacking();
        }
    }

    public static void applyEffects(Region rg){
        for(CitizensGuard guard : getOrFixGuardsFromControlPoint(rg)){
            LivingEntity entity = (LivingEntity)guard.getNPC().getEntity();
            if(entity != null){
                entity.setMaxHealth(80.0);

                //Regeneration effect not working, this is a workaround
                if(entity.getHealth() + 1 < entity.getMaxHealth() && entity.getHealth() > 0){
                    entity.setHealth(entity.getHealth() + 1);
                }

                //entity.addPotionEffect(PotionEffectType.REGENERATION.createEffect(10000, 2));
            }
        }

    }

    public static List<CitizensGuard> getOrFixGuardsFromControlPoint(Region rg){
        List<CitizensGuard> guards = new ArrayList<>();

        for(NPC npc : CitizensAPI.getNPCRegistry()){
            if(npc.getName().equalsIgnoreCase("Wächter " + rg.getName())){
                guards.add(new CitizensGuard(npc, rg));
            }
        }

        while(guards.size() < 4){
            CitizensGuard newGuard = new CitizensGuard(RandomUtils.getRandomLocation(rg.getLocation(), 5, rg.getLocation().getWorld()), rg);
            guards.add(newGuard);
        }

        return guards;
    }

    public static boolean allGuardsAreDead(Region rg){

        boolean allDead = true;

        for(CitizensGuard g : getOrFixGuardsFromControlPoint(rg)){
            if(g.getNPC().getEntity() != null && ((LivingEntity)g.getNPC().getEntity()).getHealth() > 0){
                allDead = false;
                break;
            }
        }

        return allDead;
    }

    private static void attackTarget(Region rg, Entity target){

        for(CitizensGuard guard : getOrFixGuardsFromControlPoint(rg)){
            if((!guard.isAttacking()) || (guard.getNPC().getNavigator().getEntityTarget() != null && guard.getNPC().getNavigator().getEntityTarget().getTarget().getName().equalsIgnoreCase(target.getName()))){
                guard.attack(target);
            }
        }

    }

    public static class AttackListener implements Listener {

        @EventHandler
        public void onEntityDamageByEntity(EntityDamageByEntityEvent event){

            Entity damager;

            if(event.getCause() == EntityDamageEvent.DamageCause.PROJECTILE){
                Projectile projectile = (Projectile)event.getDamager();
                damager = (Entity)projectile.getShooter();
            }else{
                damager = event.getDamager();
            }

            if(event.getEntity().hasMetadata("NPC")){
                Entity attackedNPC = event.getEntity();
                String npcName = attackedNPC.getName();
                
                if(npcName.startsWith("Wächter ")){
                    npcName = npcName.replace("Wächter ", "");

                    for(Region rg : ControlPointScheduler.rgs){
                        if(rg.getName().equalsIgnoreCase(npcName)){
                            attackTarget(rg, damager);
                        }
                    }
                }
            }
        }

    }
}
