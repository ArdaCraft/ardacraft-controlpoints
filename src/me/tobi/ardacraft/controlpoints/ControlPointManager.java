package me.tobi.ardacraft.controlpoints;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.Region;
import org.bukkit.Location;
import org.bukkit.Material;

public class ControlPointManager {
	
	public void removeControlPoint(Region rg) {
		Location loc = rg.getLocation();
		loc.add(0,-1,0);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add(1, 0, 0);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add(0,0,-1);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add(-1,0,0);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add(-1,0,0);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add(0,0,1);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add(0,0,1);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add(1,0,0);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add(1,0,0);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc = rg.getLocation();
		loc.add( 0,-1, 0);
		loc.add( 2, 0, 2);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add( 0, 0,-1);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add( 0, 0,-1);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add( 0, 0,-1);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add( 0, 0,-1);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add(-1, 0, 0);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add(-1, 0, 0);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add(-1, 0, 0);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add(-1, 0, 0);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add( 0, 0, 1);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add( 0, 0, 1);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add( 0, 0, 1);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add( 0, 0, 1);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add( 1, 0, 0);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add( 1, 0, 0);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
		loc.add( 1, 0, 0);
		loc.getWorld().getBlockAt(loc).setType(Material.AIR);
	}
	
	@SuppressWarnings("deprecation")
	public void updateState(Region rg) {
		int state = rg.getCpState();
		ArdaCraftAPI.getACDatabase().getRegionManager().update(rg);
		Location loc1 = rg.getLocation();
		setbasic(loc1.clone());
		setBase(loc1.clone());
		Location loc = loc1.clone();
		loc.getWorld().getBlockAt(loc).setType(Material.BEACON);
		
		loc.add( 0,-1, 0);
		loc.add( 2, 0, 2);
		if(state > 0)
		loc.getWorld().getBlockAt(loc).setType(Material.WOOL);
		loc.add( 0, 0,-1);
		if(state > 1)
		loc.getWorld().getBlockAt(loc).setType(Material.WOOL);
		loc.add( 0, 0,-1);
		if(state > 2)
		loc.getWorld().getBlockAt(loc).setType(Material.WOOL);
		loc.add( 0, 0,-1);
		if(state > 3)
		loc.getWorld().getBlockAt(loc).setType(Material.WOOL);
		loc.add( 0, 0,-1);
		if(state > 4)
		loc.getWorld().getBlockAt(loc).setType(Material.WOOL);
		loc.add(-1, 0, 0);
		if(state > 5)
		loc.getWorld().getBlockAt(loc).setType(Material.WOOL);
		loc.add(-1, 0, 0);
		if(state > 6)
		loc.getWorld().getBlockAt(loc).setType(Material.WOOL);
		loc.add(-1, 0, 0);
		if(state > 7)
		loc.getWorld().getBlockAt(loc).setType(Material.WOOL);
		loc.add(-1, 0, 0);
		if(state > 8)
		loc.getWorld().getBlockAt(loc).setType(Material.WOOL);
		loc.add( 0, 0, 1);
		if(state > 9)
		loc.getWorld().getBlockAt(loc).setType(Material.WOOL);
		loc.add( 0, 0, 1);
		if(state > 10)
		loc.getWorld().getBlockAt(loc).setType(Material.WOOL);
		loc.add( 0, 0, 1);
		if(state > 11)
		loc.getWorld().getBlockAt(loc).setType(Material.WOOL);
		loc.add( 0, 0, 1);
		if(state > 12)
		loc.getWorld().getBlockAt(loc).setType(Material.WOOL);
		loc.add( 1, 0, 0);
		if(state > 13)
		loc.getWorld().getBlockAt(loc).setType(Material.WOOL);
		loc.add( 1, 0, 0);
		if(state > 14)
		loc.getWorld().getBlockAt(loc).setType(Material.WOOL);
		loc.add( 1, 0, 0);
		if(state > 15)
		loc.getWorld().getBlockAt(loc).setType(Material.WOOL);
		
		loc = loc1.clone();
		loc.add( 0,-1, 0);
		loc.add( 2, 0, 2);
		if(state < 0)
			loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)7, true);
		loc.add( 0, 0,-1);
		if(state < -1)
			loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)7, true);
		loc.add( 0, 0,-1);
		if(state < -2)
			loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)7, true);
		loc.add( 0, 0,-1);
		if(state < -3)
			loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)7, true);
		loc.add( 0, 0,-1);
		if(state < -4)
			loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)7, true);
		loc.add(-1, 0, 0);
		if(state < -5)
			loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)7, true);
		loc.add(-1, 0, 0);
		if(state < -6)
			loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)7, true);
		loc.add(-1, 0, 0);
		if(state < -7)
			loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)7, true);
		loc.add(-1, 0, 0);
		if(state < -8)
			loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)7, true);
		loc.add( 0, 0, 1);
		if(state < -9)
			loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)7, true);
		loc.add( 0, 0, 1);
		if(state < -10)
			loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)7, true);
		loc.add( 0, 0, 1);
		if(state < -11)
			loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)7, true);
		loc.add( 0, 0, 1);
		if(state < -12)
			loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)7, true);
		loc.add( 1, 0, 0);
		if(state < -13)
			loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)7, true);
		loc.add( 1, 0, 0);
		if(state < -14)
			loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)7, true);
		loc.add( 1, 0, 0);
		if(state < -15)
			loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)7, true);
	}	
	
	public static ControlPointManager getInstance()  {
		return new ControlPointManager();
	}
	
	@SuppressWarnings("deprecation")
	private void setbasic(Location loc) {
		loc.add( 0,-1, 0);
		loc.add( 2, 0, 2);
		loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)8, true);
		loc.add( 0, 0,-1);
		loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)8, true);
		loc.add( 0, 0,-1);
		loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)8, true);
		loc.add( 0, 0,-1);
		loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)8, true);
		loc.add( 0, 0,-1);
		loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)8, true);
		loc.add(-1, 0, 0);
		loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)8, true);
		loc.add(-1, 0, 0);
		loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)8, true);
		loc.add(-1, 0, 0);
		loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)8, true);
		loc.add(-1, 0, 0);
		loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)8, true);
		loc.add( 0, 0, 1);
		loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)8, true);
		loc.add( 0, 0, 1);
		loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)8, true);
		loc.add( 0, 0, 1);
		loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)8, true);
		loc.add( 0, 0, 1);
		loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)8, true);
		loc.add( 1, 0, 0);
		loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)8, true);
		loc.add( 1, 0, 0);
		loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)8, true);
		loc.add( 1, 0, 0);
		loc.getWorld().getBlockAt(loc).setTypeIdAndData(35, (byte)8, true);
	}
	
	private void setBase(Location loc) {
		loc.add(0,-1,0);
		loc.getWorld().getBlockAt(loc).setType(Material.IRON_BLOCK);
		loc.add(1, 0, 0);
		loc.getWorld().getBlockAt(loc).setType(Material.IRON_BLOCK);
		loc.add(0,0,-1);
		loc.getWorld().getBlockAt(loc).setType(Material.IRON_BLOCK);
		loc.add(-1,0,0);
		loc.getWorld().getBlockAt(loc).setType(Material.IRON_BLOCK);
		loc.add(-1,0,0);
		loc.getWorld().getBlockAt(loc).setType(Material.IRON_BLOCK);
		loc.add(0,0,1);
		loc.getWorld().getBlockAt(loc).setType(Material.IRON_BLOCK);
		loc.add(0,0,1);
		loc.getWorld().getBlockAt(loc).setType(Material.IRON_BLOCK);
		loc.add(1,0,0);
		loc.getWorld().getBlockAt(loc).setType(Material.IRON_BLOCK);
		loc.add(1,0,0);
		loc.getWorld().getBlockAt(loc).setType(Material.IRON_BLOCK);
	}
	
	
	
}
