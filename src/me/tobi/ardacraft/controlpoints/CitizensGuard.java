package me.tobi.ardacraft.controlpoints;

import me.tobi.ardacraft.api.classes.Region;
import me.tobi.ardacraft.controlpoints.utils.RandomUtils;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.trait.trait.Equipment;
import net.citizensnpcs.npc.skin.SkinnableEntity;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.mcmonkey.sentinel.SentinelCurrentTarget;
import org.mcmonkey.sentinel.SentinelTrait;

import java.lang.reflect.Field;
import java.util.HashSet;


public class CitizensGuard{

    private NPC npc;

    private boolean attacking;
    private Region rg;

    public CitizensGuard(NPC npc, Region rg){
        this.npc = npc;

        attacking = false;
    }

    public CitizensGuard(Location spawnAt, Region rg){
        this.rg = rg;

        attacking = false;

        String skinName = "nox_PHX";//TODO Find better solution than using skins from actual players

        npc = CitizensAPI.getNPCRegistry().createNPC(EntityType.PLAYER, "Wächter " + rg.getName());
        npc.data().setPersistent(NPC.PLAYER_SKIN_USE_LATEST, false);
        npc.data().setPersistent(NPC.PLAYER_SKIN_UUID_METADATA, skinName);
        npc.data().setPersistent(NPC.DEFAULT_PROTECTED_METADATA, false);
        npc.data().setPersistent(NPC.RESPAWN_DELAY_METADATA, 10*60*20);//10 minutes delay
        //npc.data().setPersistent(NPC.NAMEPLATE_VISIBLE_METADATA, true);

        npc.getNavigator().getDefaultParameters().useNewPathfinder(true);

        npc.spawn(spawnAt);

        if(npc.isSpawned()){
            SkinnableEntity skinnable = npc.getEntity() instanceof SkinnableEntity ? (SkinnableEntity) npc.getEntity() : null;
            if(skinnable != null){
                skinnable.setSkinName(skinName);
            }
        }

        npc.addTrait(new SentinelTrait());
    }

    public void attack(Entity entity){
        if(npc.isSpawned()){

            equipForAttack();

            try{
                Field field = SentinelTrait.class.getDeclaredField("currentTargets");
                field.setAccessible(true);
            }catch(Exception ex){
            }

            npc.getTrait(SentinelTrait.class).addTarget(entity.getUniqueId());
            /*if(npc.getEntity().getLocation().distance(entity.getLocation()) > 5 && RandomUtils.randomInt(1, 10) < 5){
                //Attack with bow
                npc.faceLocation(entity.getLocation());

                Player npcPlayer = (Player)npc.getEntity();

                Arrow arr = npcPlayer.launchProjectile(Arrow.class);
                arr.setShooter(npcPlayer);
                arr.setBounce(true);

                //Try to make the arrow uncollectable
                try {
                    Method getHandleMethod = arr.getClass().getMethod("getHandle");
                    Object handle = getHandleMethod.invoke(arr);
                    Field fromPlayerField = handle.getClass().getField("fromPlayer");
                    fromPlayerField.set(handle, EntityArrow.PickupStatus.DISALLOWED);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }else{
                //Attack with sword
                npc.getNavigator().cancelNavigation();
                npc.getNavigator().setTarget(entity, true);
                npc.faceLocation(entity.getLocation());
            }*/
        }
    }

    public void stroll(Region rg){
        if(npc.isSpawned()){
            //npc.getNavigator().cancelNavigation();
            npc.getNavigator().setTarget(RandomUtils.getRandomLocation(rg.getLocation(), 7, rg.getLocation().getWorld()));
        }
    }

    public NPC getNPC() {
        return npc;
    }

    public boolean isAttacking() {
        return attacking;
    }

    public void clearAttacking(){
        //npc.getNavigator().cancelNavigation();

        try{
            Field field = SentinelTrait.class.getDeclaredField("currentTargets");
            field.setAccessible(true);
            ((HashSet<SentinelCurrentTarget>)field.get(npc.getTrait(SentinelTrait.class))).clear();
        }catch(Exception ex){
        }

        unequip();
        attacking = false;
    }

    private void equipForAttack(){
        npc.getTrait(Equipment.class).set(Equipment.EquipmentSlot.HELMET, new ItemStack(Material.DIAMOND_HELMET));
        npc.getTrait(Equipment.class).set(Equipment.EquipmentSlot.CHESTPLATE, new ItemStack(Material.DIAMOND_CHESTPLATE));
        npc.getTrait(Equipment.class).set(Equipment.EquipmentSlot.LEGGINGS, new ItemStack(Material.DIAMOND_LEGGINGS));
        npc.getTrait(Equipment.class).set(Equipment.EquipmentSlot.BOOTS, new ItemStack(Material.DIAMOND_BOOTS));

        npc.getTrait(Equipment.class).set(Equipment.EquipmentSlot.HAND, new ItemStack(Material.DIAMOND_SWORD));
    }

    private void unequip(){
        npc.getTrait(Equipment.class).set(Equipment.EquipmentSlot.HELMET, new ItemStack(Material.AIR));
        npc.getTrait(Equipment.class).set(Equipment.EquipmentSlot.CHESTPLATE, new ItemStack(Material.AIR));
        npc.getTrait(Equipment.class).set(Equipment.EquipmentSlot.LEGGINGS, new ItemStack(Material.AIR));
        npc.getTrait(Equipment.class).set(Equipment.EquipmentSlot.BOOTS, new ItemStack(Material.AIR));

        npc.getTrait(Equipment.class).set(Equipment.EquipmentSlot.HAND, new ItemStack(Material.AIR));
    }
}
